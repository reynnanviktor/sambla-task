'use strict';

const Hapi = require('hapi');

const jsonSwitcher = require('./jsonSwitcher.js');

const server = Hapi.server({
    port: 80,
    host: 'localhost'
});


server.route({
    method: 'GET',
    path: '/',
    handler: () => {
        return 'Send a POST request to /switcher with the json';
    }
});

server.route({
    method: 'POST',
    path: '/switcher',
    handler: (request, h) => {
        const response = h.response(jsonSwitcher(request.payload));
        response.type('application/json');
        return response;
            
    }
});

const init = async () => {

    await server.start();
    console.log(`Server running at: ${server.info.uri}`);
};

process.on('unhandledRejection', (err) => {

    console.log(err);
    process.exit(1);
});

init();