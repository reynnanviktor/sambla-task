---
title: Reynnan's Assignment
---
# Reynnan's Assignment

## Dependecies

1. [NodeJS](https://nodejs.org/en/)
2. [NPM](https://www.npmjs.com/)

*Remeber to run `npm install` before everything ;)*

## Playing

1. Run the command `npm start` _(if needed change the server port at `app.js`)_
2. Send a JSON to `localhost:port/switcher`
3. That's it, check the result

*TIP:* _execute `npm test` to run the test using jest_
### Example using postman

Sending this json to the `/switcher` endpoint:
![Sending Json](https://i.imgur.com/RQjRZdA.png)
```json
{ 
	"such a": "json",
	"what a example": {
		"this is a": "test",
		"one more": {
			"testing": "3000",
			"Leonidas": "Sparta"
		}
	},
	"number": 1,
	"array": [
		1,
		{
			"testing": "another one",
			"super test": "fantastic",
			"person": {
				"name": "Reynnan",
				"surname": "Cavalcante ALves"
			}
		},
		2
	]
}
```

Now checking the result: 
![Json response](https://i.imgur.com/OBqraOR.png)