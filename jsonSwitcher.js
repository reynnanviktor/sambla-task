const typeChecker = (value, type) => typeof value === type;

const reverseString = str => str.split("").reverse().join("");
 
const checkAndReverseString = attribute => typeChecker(attribute, "string") ? 
                                            reverseString(attribute) :
                                            attribute;

const arrayHandler = array => {
    return array.map(element => {
                if (typeChecker(element, "object")) {
                    return jsonSwitcher(element)
                }
                return element;
            });
}

module.exports = jsonSwitcher = (json, newJson = {}) => {
    try {
        Object.entries(json).map(tuple => {
            let key = tuple[0];
            let attribute = tuple[1];
            if (typeChecker(attribute, "object")) {
                Array.isArray(attribute) ?
                newJson[key] = arrayHandler(attribute) :
                newJson[key] = jsonSwitcher(attribute, newJson[key]);
            } else {                
                newJson[attribute] = checkAndReverseString(key);
            }
        });        
    } catch (error) {
        console.error("Can't use Object.entires on", json);
    }

    return newJson;
}