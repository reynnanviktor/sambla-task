const jsonSwitcher = require('./jsonSwitcher.js');

test('it should convert a "basic" json', () => {
  const json = {'test': 'test123'};
  
  const switched = jsonSwitcher(json);
  expect(switched['test123']).toBe('tset');
});

test('it should convert the json inside array', () => {
  const json = {
      'array': [
        1,
        {'json': 'convert me'},
      ]
   };

  const switched = jsonSwitcher(json);
  expect(switched['array'][0]).toBe(1);
  expect(switched['array'][1]['convert me']).toBe('nosj');
});

test('it should convert the nested objects', () => {
  const json = {
    'person': {
      'name': 'Reynnan',
      'nested': {
        'test': '123'
      }
    }
  };

  const switched = jsonSwitcher(json);
  expect(switched['person']['Reynnan']).toBe('eman')
});